const AppRoutes = {
  ROOT: "/",
  RECORDING_IN_PROGRESS: "/record",
  REVIEW_RECORDING: "/review-recording",
  EDIT_RECORDING: "/edit-recording",
  PREVIOUS_PAGE: -1
};

export default AppRoutes;
