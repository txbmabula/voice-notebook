const dateUtil = {
    convertSecondsToDigitalTime : (seconds) => {
        let time = new Date(seconds * 1000); 
        return time.toISOString().slice(11, 19).trim();
    }
}

export default dateUtil