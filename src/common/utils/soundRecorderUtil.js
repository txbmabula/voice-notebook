let audioIN = { audio: true };
//  audio is true, for recording

// Access the permission for use
// the microphone
let mediaRecorder = null;
let audioChunks = [];

let handleDataAvailable = (event) => {
  audioChunks.push(event.data);
};

window.navigator.mediaDevices.getUserMedia(audioIN).then((stream) => {
  mediaRecorder = new MediaRecorder(stream, { mimeType: "audio/webm" });
  // mediaRecorder.ondataavailable = handleDataAvailable;
  mediaRecorder.addEventListener("dataavailable", handleDataAvailable);
});

let audioClip = null;

export const soundRecorderUtil = {
  startRecording: () => {
    audioChunks = []
    if (mediaRecorder.state !== "recording") mediaRecorder.start(50);
  },
  stopRecording: async () => {
    mediaRecorder.stop();
    let localStream = await navigator.mediaDevices.getUserMedia({ audio: true });
    localStream.getTracks().forEach(track => track.stop());
  },
  pauseRecording: () => {
    mediaRecorder.pause();
  },
  resumeRecording: () => {
    mediaRecorder.resume();
  },
  obtainSoundClip: () => {
    audioClip = new Blob(audioChunks, { type: "audio/webm;" });
    let audioSrc = window.URL.createObjectURL(audioClip);

    return audioSrc;
  },
  hasSoundClip: () => {
    return audioClip !== null;
  },
};
