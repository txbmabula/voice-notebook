import { RecorderStates } from "./recorderStates";
import { RecordingActions } from "./recordingActions";
import { applyMiddleware, createStore } from "redux";
import recorderMiddleware from "../../middleware/recorderMiddleware";

let initialState = {
  recorderState: RecorderStates.INACTIVE,
  soundRecordingUrl: null,
  recordingDuration: 0,
};

let timerInterval = null;

export const RecorderReducer = (
  state = initialState,
  action
) => {
  let newState = { ...state };

  switch (action.type) {
    case RecordingActions.START:
      newState.recorderState = RecorderStates.RECORDING;
      break;
    case RecordingActions.STOP:
      newState.recorderState = RecorderStates.INACTIVE;
      newState.recordingDuration = 0;
      newState.soundRecordingUrl = action.payload;
      break;
    case RecordingActions.PAUSE:
      newState.recorderState = RecorderStates.PAUSED;
      break;
    case RecordingActions.RESUME:
      newState.recorderState = RecorderStates.RECORDING;
      break;
    case RecordingActions.UPDATE_RECORDING_DURATION:
      newState.recordingDuration = newState.recordingDuration + 1;
      break;
    default:
      break;
  }

  return newState;
};

export const startRecording = () => {
  // return { type: RecordingActions.START, payload: null };
  return async (dispatch) => {
    dispatch({ type: RecordingActions.START, payload: null })

    timerInterval = setInterval(() => {
      dispatch({type: RecordingActions.UPDATE_RECORDING_DURATION})
    }, 1000)
  }
};

export const stopRecording = () => {
    clearInterval(timerInterval)
    return { type: RecordingActions.STOP, payload: null };
};


export const pauseRecording = () => {
    clearInterval(timerInterval)
    return {type: RecordingActions.PAUSE}
}

export const resumeRecording = () => {
  return async (dispatch) => {
    dispatch({type: RecordingActions.RESUME})
    
    timerInterval = setInterval(() => {
      dispatch({type: RecordingActions.UPDATE_RECORDING_DURATION})
    }, 1000)
  }
}
