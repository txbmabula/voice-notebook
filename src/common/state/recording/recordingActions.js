export const RecordingActions = {
  STOP: "stop",
  START: "start",
  PAUSE: "pause",
  RESUME: "resume",
  UPDATE_RECORDING_DURATION: "update-recording-duration",
  UPDATE_LATEST_SOUND_RECORDING_URL: "update-latest-url"
};
