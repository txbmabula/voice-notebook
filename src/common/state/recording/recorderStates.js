export const RecorderStates = {
    RECORDING: "recording", 
    INACTIVE: "inactive",
    PAUSED: "paused",
}