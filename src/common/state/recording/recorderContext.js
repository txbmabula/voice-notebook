import { createContext } from "react";
import { RecorderStates } from "./recorderStates";

export const RecorderContext = new createContext(RecorderStates.INACTIVE);
