import { applyMiddleware, createStore } from "redux"
import recorderMiddleware from "../../middleware/recorderMiddleware"
import { RecorderReducer } from "./recorderStateManagement"
import thunk from "redux-thunk"

const ReduxStore = createStore(RecorderReducer, applyMiddleware(recorderMiddleware, thunk))

export default ReduxStore;