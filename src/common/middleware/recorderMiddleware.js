import { RecordingActions } from "../state/recording/recordingActions";
import { soundRecorderUtil } from "../utils/soundRecorderUtil";

const recorderMiddleware = () => {
  return (next) => {
    return (action) => {
      switch (action.type) {
        case RecordingActions.START:
          soundRecorderUtil.startRecording();
          break;
        case RecordingActions.STOP:
          soundRecorderUtil.stopRecording();
          action.payload = soundRecorderUtil.obtainSoundClip();
          break;
        case RecordingActions.PAUSE:
          soundRecorderUtil.pauseRecording();
          break;
        case RecordingActions.RESUME:
          soundRecorderUtil.resumeRecording();
          break;
        default:
          break;
      }
      return next(action);
    };
  };
};

export default recorderMiddleware;
