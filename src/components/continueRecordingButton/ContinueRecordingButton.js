import "./continueRecordingButton.css";

const ContinueRecordingButton = () => {
  return (
    <button className="continue-recording-button">
      <svg
        version="1.1"
        viewBox="0.0 0.0 573.509186351706 549.9002624671916"
        fill="none"
        stroke="none"
        strokeLinecap="square"
        strokeMiterlimit="10"
        xmlns="http://www.w3.org/2000/svg"
      >
        <clipPath id="p.0">
          <path
            d="m0 0l573.5092 0l0 549.90027l-573.5092 0l0 -549.90027z"
            clipRule="nonzero"
          />
        </clipPath>
        <g clipPath="url(#p.0)">
          <path
            fill="#000000"
            fillOpacity="0.0"
            d="m0 0l573.5092 0l0 549.90027l-573.5092 0z"
            fillRule="evenodd"
          />
          <path
            fill="#000000"
            fillOpacity="0.0"
            d="m182.29262 98.55779l297.70868 176.39105l-297.70868 176.39108z"
            fillRule="evenodd"
          />
          <path
            stroke="#000000"
            strokeWidth="24.0"
            strokeLinejoin="round"
            strokeLinecap="butt"
            d="m182.29262 98.55779l297.70868 176.39105l-297.70868 176.39108z"
            fillRule="evenodd"
          />
          <path
            fill="#000000"
            fillOpacity="0.0"
            d="m31.667978 274.94882l0 0c0 -140.51515 114.20617 -254.4252 255.08662 -254.4252l0 0c67.65317 0 132.53543 26.805433 180.37347 74.51942c47.837982 47.71398 74.713104 112.42802 74.713104 179.90579l0 0c0 140.51517 -114.20615 254.4252 -255.08658 254.4252l0 0c-140.88046 0 -255.08662 -113.910034 -255.08662 -254.4252z"
            fillRule="evenodd"
          />
          <path
            stroke="#000000"
            strokeWidth="24.0"
            strokeLinejoin="round"
            strokeLinecap="butt"
            d="m31.667978 274.94882l0 0c0 -140.51515 114.20617 -254.4252 255.08662 -254.4252l0 0c67.65317 0 132.53543 26.805433 180.37347 74.51942c47.837982 47.71398 74.713104 112.42802 74.713104 179.90579l0 0c0 140.51517 -114.20615 254.4252 -255.08658 254.4252l0 0c-140.88046 0 -255.08662 -113.910034 -255.08662 -254.4252z"
            fillRule="evenodd"
          />
        </g>
      </svg>
    </button>
  );
};

export default ContinueRecordingButton;
