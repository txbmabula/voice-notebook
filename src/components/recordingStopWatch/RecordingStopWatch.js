import { useState } from "react";
import { connect } from "react-redux";
import { RecorderStates } from "../../common/state/recording/recorderStates";
import dateUtil from "../../common/utils/dateUtil";
import "./recordingStopWatch.css";

const RecordingStopWatchComponent = (props) => {
  let formattedRecordingDuration = dateUtil.convertSecondsToDigitalTime(props.recordingDuration)
  return (
    <div className="recording-stop-watch">
      <time className="recording-time"> {formattedRecordingDuration} </time>
      <div className="lds-ripple" hidden={props.recorderState !== RecorderStates.RECORDING}>
        <div hidden={props.recorderState !== RecorderStates.RECORDING}></div>
        <div hidden={props.recorderState !== RecorderStates.RECORDING}></div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    recorderState: state.recorderState,
    recordingDuration: state.recordingDuration,
  };
};

const RecordingStopWatch = connect(mapStateToProps)(
  RecordingStopWatchComponent
);

export default RecordingStopWatch;
