import ResumeRecordingButton from "../resumeRecordingButton/ResumeRecordingButton";
import PauseRecordingButton from "../pauseRecordingButton/PauseRecordingButton";
import StopRecordingButton from "../stopRecordingButton/StopRecordingButton";
import "./recordingControlerGroup.css";

const RecordingControllerGroup = (props) => {
  return (
    <section className="recording-controller-group">
      {props.children}
    </section>
  );
};

export default RecordingControllerGroup;
