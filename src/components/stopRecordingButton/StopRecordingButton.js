import { connect } from "react-redux";
import AppRoutes from "../../common/navigation/appRoutes";
import { stopRecording } from "../../common/state/recording/recorderStateManagement";
import { useNavigate, useLocation } from "react-router-dom";
import "./stopRecordingButton.css";
import { useState } from "react";

let StopRecordingButtonComponent = (props) => {
  let navigate = useNavigate();
  let location = useLocation();

  let stopRecording = () => {
    props.stopRecording();
    navigate(AppRoutes.REVIEW_RECORDING + location.search);
  };

  let [isHovered, setIsHovered] = useState(false);


  return (
    <button className="recording-control-button stop-recording-button" onClick={stopRecording} onMouseEnter={() => setIsHovered(true)} onMouseLeave={() => setIsHovered(false)} title="Stop">
      <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
        viewBox="0 0 36 36">
        <path d="M35,0H1C0.448,0,0,0.447,0,1v34c0,0.553,0.448,1,1,1h34c0.552,0,1-0.447,1-1V1C36,0.447,35.552,0,35,0z"/>
      </svg>
    </button>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    stopRecording: () => dispatch(stopRecording()),
  };
};

const StopRecordingButton = connect(
  null,
  mapDispatchToProps
)(StopRecordingButtonComponent);

export default StopRecordingButton;
