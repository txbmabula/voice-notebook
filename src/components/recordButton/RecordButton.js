import { useState } from "react";
import { startRecording } from "../../common/state/recording/recorderStateManagement";
import { soundRecorderUtil } from "../../common/utils/soundRecorderUtil";
import { connect } from "react-redux";
import { useNavigate, useLocation } from "react-router-dom";
import "./recordButton.css";
import AppRoutes from "../../common/navigation/appRoutes";

let RecordButtonComponent = (props) => {
  let navigate = useNavigate();
  let location = useLocation();

  let startRecording = () => {
    props.startRecording()
    navigate(AppRoutes.RECORDING_IN_PROGRESS + location.search);
  };

  return (
    <button className="record-button" title="Start recording" onClick={startRecording} >
      <svg className="record-icon-svg" viewBox="0 0 100 100">
        <circle className="record-icon-outer-circle" cx="50" cy="50"/>
        <circle className="record-icon-inner-circle" cx="50" cy="50"/>
        <text className="record-icon-text" x="50%" y="50%">
          Record
        </text>
      </svg>
    </button>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    startRecording: () => dispatch(startRecording()),
  };
};

let RecordButton = connect(null, mapDispatchToProps)(RecordButtonComponent);

export default RecordButton;
