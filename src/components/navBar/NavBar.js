import "./navBar.css";
import { NavLink } from "react-router-dom";
import AppRoutes from "../../common/navigation/appRoutes";

let NavBar = () => {
  return (
    <nav>
      <strong><NavLink to={AppRoutes.ROOT}> Voice Notebook </NavLink></strong>
    </nav>
  );
};

export default NavBar;
