import { connect } from "react-redux";
import { pauseRecording } from "../../common/state/recording/recorderStateManagement";
import { RecorderStates } from "../../common/state/recording/recorderStates";
import "./pauseRecordingButton.css";

const PauseRecordingButtonComponent = (props) => {
  let isDisabled = props.recorderState === RecorderStates.PAUSED

  return (
    <button className="recording-control-button pause-recording-button" title="Pause" onClick={props.pauseRecording} disabled={isDisabled}>
      <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
        viewBox="0 0 365 365">
        <g>
          <rect x="74.5" width="73" height="365"/>
          <rect x="217.5" width="73" height="365"/>
        </g>
      </svg>
    </button>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    pauseRecording: () => {dispatch(pauseRecording())}
  }
}

const mapStateToProps = (state) => {
  return {
    recorderState: state.recorderState
  }
}

const PauseRecordingButton = connect(mapStateToProps, mapDispatchToProps)(PauseRecordingButtonComponent)

export default PauseRecordingButton;
