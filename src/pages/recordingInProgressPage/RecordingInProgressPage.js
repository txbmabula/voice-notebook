import { useEffect } from "react";
import { connect } from "react-redux";
import { startRecording } from "../../common/state/recording/recorderStateManagement";
import { soundRecorderUtil } from "../../common/utils/soundRecorderUtil";
import PauseRecordingButton from "../../components/pauseRecordingButton/PauseRecordingButton";
import RecordingControllerGroup from "../../components/recordingControlerGroup/RecordingControlerGroup";
import RecordingStopWatch from "../../components/recordingStopWatch/RecordingStopWatch";
import ResumeRecordingButton from "../../components/resumeRecordingButton/ResumeRecordingButton";
import StopRecordingButton from "../../components/stopRecordingButton/StopRecordingButton";
import "./recordingInProgressPage.css";

const RecordingInProgressPageComponent = (props) => {

  return (
    <section className="recording-in-progress-main-section">
      <RecordingStopWatch></RecordingStopWatch>
      <RecordingControllerGroup>
        <PauseRecordingButton />
        <StopRecordingButton />
        <ResumeRecordingButton />
      </RecordingControllerGroup>
    </section>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    startRecording: () => dispatch(startRecording()),
  };
};

const RecordingInProgressPage = connect(
  null,
  mapDispatchToProps
)(RecordingInProgressPageComponent);

export default RecordingInProgressPage;
