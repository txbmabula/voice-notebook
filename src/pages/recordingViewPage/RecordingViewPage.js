import { connect } from "react-redux";
import EditRecordingButton from "../../components/editRecordingButton/EditRecordingButton";
import NewRecordingButton from "../../components/newRecordingButton/NewRecordingButton";
import RecordingControllerGroup from "../../components/recordingControlerGroup/RecordingControlerGroup";
import SaveRecordingButton from "../../components/saveRecordingButton/SaveRecordingButton";
import "./recordingViewPage.css";

const RecordingViewPageComponent = (props) => {
  return (
    <section id="recording-view-main-section">
      <section className="playback-audio-section">
        <h1 className="playback-audio-heading"> Playback </h1>
        <audio
          controls
          id="playback-audio-comp"
          src={props.audioRecordingUrl}
          type="audio/mp3"
        ></audio>
      </section>
      <RecordingControllerGroup>
        <NewRecordingButton />
        <EditRecordingButton />
        <SaveRecordingButton />
      </RecordingControllerGroup>
    </section>
  );
};

const mapStateToProps = (state) => {
  return {
    audioRecordingUrl: state.soundRecordingUrl
  };
};
const RecordingViewPage = connect(mapStateToProps)(RecordingViewPageComponent);

export default RecordingViewPage;
