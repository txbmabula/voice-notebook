import NavBar from "../../components/navBar/NavBar";
import RecordButton from "../../components/recordButton/RecordButton";
// import StopRecordingButton from "../../components/stopRecordingButton/StopRecordingButton";
import { soundRecorderUtil } from "../../common/utils/soundRecorderUtil";
import "./voiceRecordPage.css";
import { useState } from "react";
import { RecorderContext } from "../../common/state/recording/recorderContext";
import StopRecordingButton from "../../components/stopRecordingButton/StopRecordingButton";
import { RecorderStates } from "../../common/state/recording/recorderStates";
import { connect } from "react-redux";

const VoiceRecordPage = (props) => {
  return (
    <div>
      <section className="voice-record-main-section">
        <RecordButton />
      </section>
    </div>
  );
};

export default VoiceRecordPage;
