import "./pageUnderConstruction.css"
import { useNavigate, useLocation } from "react-router-dom";
import AppRoutes from "../../common/navigation/appRoutes";

const PageUnderConstruction = () => {

    let location = useLocation();
    let navigate = useNavigate();

    let returnToPrevPage = () => {
        navigate(AppRoutes.PREVIOUS_PAGE)
    }
    return (
        <section className="page-under-construction-section">
            <h1> Page Under Construction </h1>
            <p> 
                This feature is still under-constuction, but we're working on getting it up
                and running ASAP! 
            </p>
            <button className="back-to-prev-page-btn" onClick={returnToPrevPage}>
                Go back to previous page
            </button>
        </section>
    )
}

export default PageUnderConstruction;