import "./App.css";
import React from 'react';
import {connect} from 'react-redux';
import NavBar from "./components/navBar/NavBar";
import RecordingInProgressPageComponent from "./pages/recordingInProgressPage/RecordingInProgressPage";
import VoiceRecordPage from "./pages/voiceRecordPage/VoiceRecordPage";
import { Routes, Route, Navigate } from "react-router-dom";
import AppRoutes from "./common/navigation/appRoutes";
import RecordingViewPage from "./pages/recordingViewPage/RecordingViewPage";
import { RecorderStates } from "./common/state/recording/recorderStates";
import PageUnderConstruction from "./pages/pageUnderConstruction/PageUnderConstruction";

const AppComponent = (props) => {

  let recordingInProgressElement = props.recorderState === RecorderStates.INACTIVE ? < Navigate to={AppRoutes.ROOT} /> : <RecordingInProgressPageComponent /> ;

  return (
    <div>
      <NavBar></NavBar>
      <Routes>
        <Route path={AppRoutes.ROOT} element={<VoiceRecordPage />} />
        <Route
          path={AppRoutes.RECORDING_IN_PROGRESS}
          element={recordingInProgressElement}
        />
        <Route path={AppRoutes.REVIEW_RECORDING} element={<RecordingViewPage />} />
        <Route path={AppRoutes.EDIT_RECORDING} element={<PageUnderConstruction />} />
      </Routes> 
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    recorderState: state.recorderState
  }
}

const App = connect(mapStateToProps)(AppComponent)

export default App;
